/**
* A cool boat entity
*/
component persistent="true" joinColumn="TID" table="bluFish__boat"{

	// Primary Key
	property name="id" column="BOATID" fieldtype="id" generator="uuid" setter="false";
	
	// Properties

	
	property name="number"
		     ormtype="string";
	
	property name="lastname"
		     ormtype="string";
	
	property name="firstname"
		     ormtype="string";
	
	property name="address"
		     ormtype="string";
	
	property name="city"
		     ormtype="string";
	
	property name="state"
		     ormtype="string";
	
	property name="zip"
		     ormtype="string";
	
	property name="email"
		     ormtype="string";
	
	property name="phone"
		     ormtype="string";
	
	property name="cell"
		     ormtype="string";
	
	property name="fax"
		     ormtype="string";
	
	property name="boat"
		     ormtype="string";
	
	property name="homeport"
		     ormtype="string";
	
	property name="boatmake"
		     ormtype="string";
	
	property name="boatlength"
		     ormtype="string";
	
	property name="boatbeam"
		     ormtype="string";
	
	property name="boatyearbuilt"
		     ormtype="string";
	
	property name="enginemake"
		     ormtype="string";
	
	property name="enginemodel"
		     ormtype="string";
	
	property name="enginepower"
		     ormtype="string";
	
	property name="approved"
			 ormtype="boolean" 
			 length="4";
	
	property name="shorepower"
		     ormtype="string";
	
	property name="websiteorder"
		     ormtype="double";
	
	property name="slip"
		     ormtype="string";



     property name="Participants" singularname="Participant" fieldtype="one-to-many" cfc="Participant" fkcolumn="BOATID" inverse=true cascade="all-delete-orphan";

  
	 property name="Tournament" column="TID" fieldtype="many-to-one" cfc="Tournament" fkcolumn="TID";

	
	// Validation
	this.constraints = {
		// Example: age = { required=true, min="18", type="numeric" }
	};
	
	// Constructor
	function init(){
		
		return this;
	}
	function getName() {
		return uCase( variables.name );
	}

}

