/**
* A cool tournament entity
*/
component persistent="true" joinColumn="TID" table="bluFish__category"{

	// Primary Key
	property name="id" column="CID" fieldtype="id" generator="uuid" setter="false";
	
	// Properties
	property name="categoryname" ormtype="string";
	property name="whichorder" ormtype="double";
	property name="maxplaces" ormtype="double";
	property name="tournamentsplits" ormtype="string";
	property name="optionalsplits" ormtype="string";
	property name="maxoptionalplaces" ormtype="double";
	

	property name="tournament" column="TID" fieldtype="many-to-one" cfc="tournament" fkcolumn="TID";
	property name="Optionals" singularname="Optional" fieldtype="one-to-many" cfc="Optional" fkcolumn="OID";
	
	// Validation
	this.constraints = {
		// Example: age = { required=true, min="18", type="numeric" }
	};
	
	// Constructor
	function init(){
		
		return this;
	}
}

