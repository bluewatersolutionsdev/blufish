/**
* A cool tournament entity
*/
component persistent="true" table="bluFish__tournament"{

	// Primary Key
	property name="id" column="TID" fieldtype="id" generator="uuid" setter="false";
	
	// Properties
	property name="tournamentname" ormtype="string";
	property name="tournamentyear" ormtype="string";
	property name="maxoptionallevels" ormtype="double";
	property name="entryfeetotal" ormtype="double";
	property name="entryfee" ormtype="double";
	property name="entryfeecut" ormtype="double";
	property name="optionalfeecut" ormtype="double";
	property name="active" ormtype="boolean" length="4";
	property name="whichorder" ormtype="double";
	property name="taxrate" ormtype="double";
	property name="creditcardfee" ormtype="double";
	property name="legalattachment" ormtype="string";
	property name="tournamentstyle" ormtype="string";
	property name="fishmultiplier" ormtype="double";
	property name="fishminimum" ormtype="double";
	property name="displaylocked"  ormtype="boolean" length="4";

	

	property name="Boats" singularname="Boat" fieldtype="one-to-many" cfc="Boat" fkcolumn="TID" inverse=true cascade="all-delete-orphan";
	
	
	// Validation
	this.constraints = {
		// Example: age = { required=true, min="18", type="numeric" }
	};
	
	// Constructor
	function init(){
		
		return this;
	}
}

