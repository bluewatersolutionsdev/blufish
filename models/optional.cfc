/**
* A cool tournament entity
*/
component persistent="true" joinColumn="CID" table="bluFish__optional"{

	// Primary Key
	property name="id" column="OID" fieldtype="id" generator="uuid" setter="false";
	
	// Properties
	property name="amount" ormtype="double";
	property name="title" ormtype="string";
	property name="whichorder" ormtype="double";
	property name="maxplaces" ormtype="double";
	property name="splits" ormtype="string";
	property name="requirements" ormtype="string";

	

	property name="category" column="CID" fieldtype="one-to-one" cfc="category" fkcolumn="CID";
	
	
	// Validation
	this.constraints = {
		// Example: age = { required=true, min="18", type="numeric" }
	};
	
	// Constructor
	function init(){
		
		return this;
	}
}

