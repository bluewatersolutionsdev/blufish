/**
* A cool angler entity
*/
component persistent="true" joinColumn="boatid"  table="bluFish__participant"{

	// Primary Key
	property name="id" fieldtype="id" column="PARTICIPANTID" generator="uuid" setter="false";
	
	// Properties
	property name="type" column="type" ormtype="string";
	property name="firstname" column="firstname" ormtype="string";
	property name="lastname" column="lastname" ormtype="string";
	property name="address" column="address" ormtype="string";
	property name="city" column="city" ormtype="string";
	property name="state" column="state" ormtype="string";
	property name="zip" column="zip" ormtype="string";
	property name="email" column="email" ormtype="string";
	property name="phone" column="phone" ormtype="string";
	property name="createdAt" ormtype="timestamp" update="false";
	property name="updatedAt" ormtype="timestamp";
	




	property name="boat" column="BOATID" fieldtype="many-to-one" cfc="boat" fkcolumn="BOATID";

	// Validation
	this.constraints = {
		// Example: age = { required=true, min="18", type="numeric" }
		
	};
	
	// Constructor
	function init(){
		
		createdAt		= now();
		updatedAt		= now();
		return this;
	}
}

