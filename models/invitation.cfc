/**
* A cool tournament entity
*/
component persistent="true"  table="bluFish__invitation"{

	// Primary Key
	property name="id" column="InvitationID" fieldtype="id" generator="uuid" setter="false";
	
	// Properties
	property name="invitationcode" ormtype="string";
	property name="expirationdate" ormtype="timestamp";

	

	property name="tournament" column="TID" fieldtype="many-to-one" cfc="tournament" fkcolumn="TID";
	property name="boat" column="BOATID" fieldtype="many-to-one" cfc="boat" fkcolumn="BOATID";
	
	
	// Validation
	this.constraints = {
		// Example: age = { required=true, min="18", type="numeric" }
	};
	
	// Constructor
	function init(){
		
		return this;
	}
}

