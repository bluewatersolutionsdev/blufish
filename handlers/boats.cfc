/**
* Manage boats
* It will be your responsibility to fine tune this template, add validations, try/catch blocks, logging, etc.
*/
component extends="coldbox.system.EventHandler"{
	
	// DI Virtual Entity Service
	property name="ormService" inject="entityService:boat";
	
	// HTTP Method Security
	this.allowedMethods = {
		index = "GET", 
		new = "GET", 
		edit = "GET", 
		delete = "POST,DELETE", 
		save = "POST,PUT"
	};
	
	/**
	* preHandler()
	*/
	function preHandler( event, rc, prc ){
		event.paramValue( "format", "html" );
		//writeDump(ormService);
	}
		
	/**
	* Listing
	*/
	function index( event, rc, prc ){
		// Get all boats
		prc.boats = ormService.getAll();
		//tournamentObj = entityLoad( 'tournament', rc.tid, true );
		//prc.boats = ormService.findALLWhere(entityName="tournament",criteria={tournament=tournamentObj});
		//writeDump(prc.boats);
		// Multi-format rendering
		// event.renderData( data=prc.boats, formats="xml,json,html,pdf" );
		//writeDump(prc.boats);
		event.setView( "boats/index" );
	}	
	
	/**
	* New Form
	*/
	function new( event, rc, prc ){
		// get new boat
		prc.boat = ormService.new();
		
		event.setView( "boats/new" );
		//event.setView(view="boats/new", layout="ajax" );

	}	

	/**
	* Edit Form
	*/
	function edit( event, rc, prc ){
		// get persisted boat
		prc.boat = ormService.get( rc.boatid );
		
		event.setView( "boats/edit" );

	}	
	
	/**
	* View boat mostly used for RESTful services only.
	*/
	function show( event, rc, prc ){
		// Default rendering.
		event.paramValue( "format", "json" );
		// Get requested entity by id
		prc.boat = ormService.get( rc.boatid );
		// Multi-format rendering
		event.renderData( data=prc.boat, formats="xml,json" );
	}

	/**
	* Save and Update
	*/
	function save( event, rc, prc ){
		// get boat to persist or update and populate it with incoming form
		prc.boat = populateModel( model=ormService.get( rc.id ), exclude="id", composeRelationships=true );
		
		// Do your validations here
		
		// Save it
		ormService.save( prc.boat );
		
		// RESTful Handler
		switch(rc.format){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				event.renderData( data=prc.boat, type=rc.format, location="/boats/show/#prc.boat.getboatid()#" );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="boat Saved", type="success" } );
				// Redirect to listing
				setNextEvent( event='cbadmin.module.bluFish.boats.edit', queryString="tid=#rc.tid#&boatid=#prc.boat.getid()#&tab=tParticipants" );
				
	abort;

			}
		}
	}	

	/**
	* Delete
	*/
	function delete( event, rc, prc ){
		// Delete record by ID
		var removed = ormService.delete( ormService.get( rc.id ) );
		
		// RESTful Handler
		switch( rc.format ){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				var restData = { "deleted" = removed };
				event.renderData( data=restData, type=rc.format );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="Boat Deleted!", type="success" } );
				// Redirect to listing
				setNextEvent( 'cbadmin.module.bluFish.boats.index',queryString="tid=#rc.tid#" );
			}
		}
	}	
	
}
