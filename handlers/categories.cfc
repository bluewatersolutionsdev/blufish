/**
* Manage categories
* It will be your responsibility to fine tune this template, add validations, try/catch blocks, logging, etc.
*/
component extends="coldbox.system.EventHandler"{
	
	// DI Virtual Entity Service
	property name="ormService" inject="entityService:category";
	
	// HTTP Method Security
	this.allowedMethods = {
		index = "GET", 
		new = "GET", 
		edit = "GET", 
		delete = "POST,DELETE", 
		save = "POST,PUT"
	};
	
	/**
	* preHandler()
	*/
	function preHandler( event, rc, prc ){
		event.paramValue( "format", "html" );
	}
		
	/**
	* Listing
	*/
	function index( event, rc, prc ){
		// Get all categories
		prc.categories = ormService.getAll();
		// Multi-format rendering
		// event.renderData( data=prc.categories, formats="xml,json,html,pdf" );
		event.setView( "categories/index" );
	}	
	
	/**
	* New Form
	*/
	function new( event, rc, prc ){
		// get new category
		prc.category = ormService.new();
		writeDump(rc);
		event.setView( "categories/new" );

	}	

	/**
	* Edit Form
	*/
	function edit( event, rc, prc ){
		// get persisted category
		prc.category = ormService.get( rc.id );
		
		event.setView( "categories/edit" );
	}	
	
	/**
	* View category mostly used for RESTful services only.
	*/
	function show( event, rc, prc ){
		// Default rendering.
		event.paramValue( "format", "json" );
		// Get requested entity by id
		prc.category = ormService.get( rc.id );
		// Multi-format rendering
		event.renderData( data=prc.category, formats="xml,json" );
	}

	/**
	* Save and Update
	*/
	function save( event, rc, prc ){
		// get category to persist or update and populate it with incoming form
		prc.category = populateModel( model=ormService.get( rc.id ), exclude="id", composeRelationships=true );
		
		// Do your validations here
		
		// Save it
		//writeDump(prc.category);abort;
		ormService.save( prc.category );
		
		// RESTful Handler
		switch(rc.format){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				event.renderData( data=prc.category, type=rc.format, location="/categories/show/#prc.category.getid()#" );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="category Saved", type="success" } );
				// Redirect to listing
				setNextEvent( event='cbadmin.module.bluFish.categories.index',queryString="tid=#rc.tournament#" );

			}
		}
	}	

	/**
	* Delete
	*/
	function delete( event, rc, prc ){
		// Delete record by ID
		var removed = ormService.delete( ormService.get( rc.id ) );
		
		// RESTful Handler
		switch( rc.format ){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				var restData = { "deleted" = removed };
				event.renderData( data=restData, type=rc.format );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="category Deleted!", type="success" } );
				// Redirect to listing
				setNextEvent( 'cbadmin.module.bluFish.categories' );
			}
		}
	}	
	
}
