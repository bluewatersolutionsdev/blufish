/**
* Manage participants
* It will be your responsibility to fine tune this template, add validations, try/catch blocks, logging, etc.
*/
component extends="coldbox.system.EventHandler"{
	
	// DI Virtual Entity Service
	property name="ormService" inject="entityService:participant";
	property name="boatService" inject="entityService:boat";
	
	// HTTP Method Security
	this.allowedMethods = {
		index = "GET", 
		new = "GET", 
		edit = "GET", 
		delete = "POST,DELETE", 
		save = "POST,PUT"
	};
	
	/**
	* preHandler()
	*/
	function preHandler( event, rc, prc ){
		event.paramValue( "format", "html" );
		//prc.boats=boatService.getall();
	}
		
	/**
	* Listing
	*/
	function index( event, rc, prc ){
		// Get all participants
		//prc.participants = ormService.getall();

		//writedump(rc);
		boatObj = entityLoad( 'boat', rc.boatid, true );
		prc.participants = ormService.findALLWhere(entityName="boat",criteria={boat=boatObj});

		//writeDump(boatObj);
		//writeOutput("<br><br>ormService.findAllWhere(entityName=""boat"", criteria={boat=boatobj})");
		//writeDump(ormService.findWhere(entityName="boat",criteria={boat=boatobj}));




		//writeOutput("<br><br>prc.participants:");
		//writeDump(prc.participants);


		
	
		//writeDump(prc.participants);



		//writeDump(ormService.findAllWhere(entityName="boat", criteria={"BOATID"=rc.boatid}));abort;
		//writeDump(prc.participants[1].getboat().getboatid(rc.boatid));abort;
//writeDump(ormservice.getall().getboatid(rc.boatid));abort;		
//		writeDump(ormservice.findAllWhere(entityName="boat", criteria={"boat"=rc.boatid}));abort;
		//writeDump(rc.boatid);
		//writeDump(ormService.findWhere(entityName="BOAT", criteria={"ID"=rc.boatid}));abort;
		// Multi-format rendering
		// event.renderData( data=prc.participants, formats="xml,json,html,pdf" );
		//event.setView( "participants/index" );
		event.setView(view="participants/index", layout="ajax" );
	}	
	
	/**
	* New Form
	*/
	function new( event, rc, prc ){
		// get new participant
		prc.participant = ormService.new();
		
		//event.setView( "participants/new" );
		event.setView(view="participants/new", layout="ajax" );

	}	

	/**
	* Edit Form
	*/
	function edit( event, rc, prc ){
		// get persisted participant
		prc.participant = ormService.get( rc.id );
		
		//event.setView( "participants/edit" );
		event.setView(view="participants/edit", layout="ajax" );
	}	
	
	/**
	* View participant mostly used for RESTful services only.
	*/
	function show( event, rc, prc ){
		// Default rendering.
		event.paramValue( "format", "json" );
		// Get requested entity by id
		prc.participant = ormService.get( rc.id );
		// Multi-format rendering
		event.renderData( data=prc.participant, formats="xml,json" );
	}

	/**
	* Save and Update
	*/
	function save( event, rc, prc ){
		// get participant to persist or update and populate it with incoming form
		prc.participant = populateModel( model=ormService.get( rc.id ), exclude="id", composeRelationships=true );
		
		// Do your validations here
		writeDump(prc.participant);
		// Save it
		ormService.save( prc.participant );
		
		// RESTful Handler
		switch(rc.format){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				event.renderData( data=prc.participant, type=rc.format, location="/participants/show/#prc.participant.getid()#" );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="participant Saved", type="success" } );
				// Redirect to listing
				//setNextEvent( 'cbadmin.module.bluFish.participants' );
				setNextEvent( event='cbadmin.module.bluFish.boats.edit', queryString="tid=#rc.tid#&boatid=#rc.boat#&tab=tParticipants" );
			}
		}
	}	

	/**
	* Delete
	*/
	function delete( event, rc, prc ){

		// Delete record by ID
		var removed = ormService.delete( ormService.get( rc.id ) );
		
		// RESTful Handler
		switch( rc.format ){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				var restData = { "deleted" = removed };
				event.renderData( data=restData, type=rc.format );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="participant Deleted!", type="success" } );
				// Redirect to listing
				//setNextEvent( 'cbadmin.module.bluFish.participants' );
				setNextEvent( event='cbadmin.module.bluFish.boats.edit', queryString="tid=#rc.tid#&boatid=#rc.boatid#&tab=tParticipants" );
			}
		}
	}	
	
}
