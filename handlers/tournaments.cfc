/**
* Manage tournaments
* It will be your responsibility to fine tune this template, add validations, try/catch blocks, logging, etc.
*/
component extends="coldbox.system.EventHandler"{
	
	// DI Virtual Entity Service
	property name="ormService" inject="entityService:tournament";
	
	// HTTP Method Security
	this.allowedMethods = {
		index = "GET", 
		new = "GET", 
		edit = "GET", 
		delete = "POST,DELETE", 
		save = "POST,PUT"
	};
	
	/**
	* preHandler()
	*/
	function preHandler( event, rc, prc ){
		event.paramValue( "format", "html" );
	}
		
	/**
	* Listing
	*/
	function index( event, rc, prc ){
		// Get all tournaments
		prc.tournaments = ormService.getAll();
		// Multi-format rendering
		// event.renderData( data=prc.tournaments, formats="xml,json,html,pdf" );
		event.setView( "tournaments/index" );
	}	
	
	/**
	* New Form
	*/
	function new( event, rc, prc ){
		// get new tournament
		prc.tournament = ormService.new();
		
		event.setView( "tournaments/new" );

	}	

	/**
	* Edit Form
	*/
	function edit( event, rc, prc ){
		// get persisted tournament
		prc.tournament = ormService.get( rc.id );
		
		event.setView( "tournaments/edit" );
	}	
	
	/**
	* View tournament mostly used for RESTful services only.
	*/
	function show( event, rc, prc ){
		// Default rendering.
		event.paramValue( "format", "json" );
		// Get requested entity by id
		prc.tournament = ormService.get( rc.id );
		// Multi-format rendering
		event.renderData( data=prc.tournament, formats="xml,json" );
	}

	/**
	* Save and Update
	*/
	function save( event, rc, prc ){
		// get tournament to persist or update and populate it with incoming form
		prc.tournament = populateModel( model=ormService.get( rc.id ), exclude="id", composeRelationships=true );
		
		// Do your validations here
		writeDump(prc.tournament);
		// Save it
		ormService.save( prc.tournament );
		
		// RESTful Handler
		switch(rc.format){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				event.renderData( data=prc.tournament, type=rc.format, location="/tournaments/show/#prc.tournament.getid()#" );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="tournament Saved", type="success" } );
				// Redirect to listing
				setNextEvent( 'cbadmin.module.bluFish.tournaments' );

			}
		}
	}	

	/**
	* Delete
	*/
	function delete( event, rc, prc ){
		// Delete record by ID
		var removed = ormService.delete( ormService.get( rc.id ) );
		
		// RESTful Handler
		switch( rc.format ){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				var restData = { "deleted" = removed };
				event.renderData( data=restData, type=rc.format );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="tournament Deleted!", type="success" } );
				// Redirect to listing
				setNextEvent( 'cbadmin.module.bluFish.tournaments' );
			}
		}
	}	
	
}
