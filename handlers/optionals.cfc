/**
* Manage optionals
* It will be your responsibility to fine tune this template, add validations, try/catch blocks, logging, etc.
*/
component extends="coldbox.system.EventHandler"{
	
	// DI Virtual Entity Service
	property name="ormService" inject="entityService:optional";
	
	// HTTP Method Security
	this.allowedMethods = {
		index = "GET", 
		new = "GET", 
		edit = "GET", 
		delete = "POST,DELETE", 
		save = "POST,PUT"
	};
	
	/**
	* preHandler()
	*/
	function preHandler( event, rc, prc ){
		event.paramValue( "format", "html" );
	}
		
	/**
	* Listing
	*/
	function index( event, rc, prc ){
		// Get all optionals
		prc.optionals = ormService.getAll();
		//categoryObj = entityLoad( 'category',rc.cid, true );
		//writeDump(rc);
		//writeDump(categoryObj);
		//writedump(prc.optionals);
		//abort;
		//prc.optionals = ormService.findALLWhere(entityName="category",criteria={category=categoryObj});

		// Multi-format rendering
		// event.renderData( data=prc.optionals, formats="xml,json,html,pdf" );
		event.setView( "optionals/index" );
	}	
	
	/**
	* New Form
	*/
	function new( event, rc, prc ){
		// get new optional
		prc.optional = ormService.new();
		
		event.setView( "optionals/new" );

	}	

	/**
	* Edit Form
	*/
	function edit( event, rc, prc ){
		// get persisted optional
		prc.optional = ormService.get( rc.id );
		
		event.setView( "optionals/edit" );
	}	
	
	/**
	* View optional mostly used for RESTful services only.
	*/
	function show( event, rc, prc ){
		// Default rendering.
		event.paramValue( "format", "json" );
		// Get requested entity by id
		prc.optional = ormService.get( rc.id );
		// Multi-format rendering
		event.renderData( data=prc.optional, formats="xml,json" );
	}

	/**
	* Save and Update
	*/
	function save( event, rc, prc ){
		// get optional to persist or update and populate it with incoming form
		prc.optional = populateModel( model=ormService.get( rc.id ), exclude="id", composeRelationships=true );
		
		// Do your validations here
		
		// Save it
		writeDump(rc);
		writeDump(prc.optional);
		ormService.save( prc.optional );
		
		// RESTful Handler
		switch(rc.format){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				event.renderData( data=prc.optional, type=rc.format, location="/optionals/show/#prc.optional.getid()#" );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="optional Saved", type="success" } );
				// Redirect to listing
				//setNextEvent( event='cbadmin.module.bluFish.optionals.index',queryString="tid=#rc.tournament#" );

			}
		}
	}	

	/**
	* Delete
	*/
	function delete( event, rc, prc ){
		// Delete record by ID
		var removed = ormService.delete( ormService.get( rc.id ) );
		
		// RESTful Handler
		switch( rc.format ){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				var restData = { "deleted" = removed };
				event.renderData( data=restData, type=rc.format );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="optional Deleted!", type="success" } );
				// Redirect to listing
				setNextEvent( 'cbadmin.module.bluFish.optionals' );
			}
		}
	}	
	
}
