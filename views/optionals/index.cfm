<cfoutput>
<div class="row hidden-sm">
    <div class="col-md-6">
        <h2 class="h3"><a href=""><i class="fa fa-home"></i> </a>  Optionals</h2>
    </div>
    <div class="col-md-6">
        <!--- Create Button --->
#html.href( 
  href="cbadmin.module.bluFish.optionals.new", 
  queryString="tid=#rc.tid#&cid=#rc.cid#", 
  text="Create optional", 
  class="btn btn-primary pull-right"
)#
    </div>
</div>


<!--- MessageBox --->
<cfif flash.exists( "notice" )>
<div class="row">
    <div class="col-md-12">
	    <div class="alert alert-#flash.get( "notice" ).type#">
	        #flash.get( "notice" ).message#
	    </div>
    </div>
</div>
</cfif>



<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
<!--- Listing --->

<table class="sortable table table-striped table-hover table-condensed" name="crudlist" id="crudlist" >
	<thead>
		<tr>
		
			<th>amount</th>
			
			
			<th>maxplaces</th>
			<th>requirements</th>
			<th>splits</th>
			<th>title</th>
			<th>whichorder</th>
			<th width="150">Actions</th>
		</tr>
	</thead>
	<tbody>
		<cfloop array="#prc.optionals#" index="thisRecord">
		<tr>
			
					<td>#thisRecord.getamount()#</td>
					<td>#thisRecord.getmaxplaces()#</td>
					<td>#thisRecord.getrequirements()#</td>
					<td>#thisRecord.getsplits()#</td>
					<td>#thisRecord.gettitle()#</td>
					<td>#thisRecord.getwhichorder()#</td>
			
			<td>
			<div class="btn-group pull-right">
			#html.startForm(action="cbadmin.module.bluFish.optionals.delete", id="modalFormDelete")#
				#html.button(value="<i class='fa fa-edit'></i> Delete", onclick="return confirm('Really Delete Record?')", class="btn btn-danger btn-sm", type="submit")# 		
        #html.href(href="cbadmin.module.bluFish.optionals.edit", queryString="tid=#rc.tid#&id=#thisRecord.getid()#", text="<i class='fa fa-edit'></i> Edit", class="btn btn-primary btn-sm")#
				
				
				#html.hiddenField(name="id", bind=thisRecord)#
			#html.endForm()#
			</div>
			</td>
		</tr>
		</cfloop>
	</tbody>
</table>


            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function(){

   var table = $('.sortable').DataTable({
       fixedHeader: true,
        stateSave: true,
        responsive: true
      });

    
    
    /*!
     * Modal placeholder action
     */
    $('##btnSave').click(function(event){
        $('##modalForm').submit();
        return false;
    });

    /*!
     * Modal placeholder for delete
     */
    $("##myModalDelete").on("show.bs.modal", function(e) {
 
    });

    /*!
     * Modal placeholder delete action
     */
    $('##btnDelete').click(function(event){
        $('##modalFormDelete').submit();
        return false;
    });



        
});


</script>







<div id="myModalDelete" class="modal fade modal-danger" role="dialog">
  <div class="modal-dialog" style="width:500px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Delete</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this item?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button name="btnDelete" type="button" class="btn btn-danger"  id="btnDelete">Delete</button>
      </div>
    </div>

  </div>
</div>
</cfoutput>