<!--- 
	EntityField Comment:
	- textAreas = A list of properties that are textareas
	- booleanSelect = If true creates a select box, else two radio buttons
	- manyToOne = { manyToOnePropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder=""} }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string]. Example: {criteria={productid=1},sortorder='Department desc'}
	- ManyToMany = { manyToManyPropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder="",selectColumn='' }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string,selectColumn='']. Example: {criteria={productid=1},sortorder='Department desc'}
	- showRelations = If true (default) will show one to many and one to one relations as a view table snapshot
--->

<cfoutput>
<div class="row">
    <div class="col-md-12">
        <h2 class="h3"><a href=""><i class="fa fa-home"></i> optionals:</a> #args.title#</h2>
    </div>
</div>

<!--- Submit Form --->
#html.startForm( action='cbadmin.module.bluFish.optionals.save' )#
	


	#html.textField(
	    name="amount",
	    label="Amount:",
	    bind=prc.optional,
	    class="form-control",
	    title="Amount",
	    placeholder="Amount",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="amount",
	    groupWrapper="div class=form-group"
	)#





	#html.hiddenField(
	    name="id",
	    bind=prc.optional
	)#
	
	#html.hiddenField(
	    name="category",
	    bind=prc.optional
	)#


	#html.textField(
	    name="maxplaces",
	    label="Maxplaces:",
	    bind=prc.optional,
	    class="form-control",
	    title="Maxplaces",
	    placeholder="Maxplaces",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="maxplaces",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="requirements",
	    label="Requirements:",
	    bind=prc.optional,
	    class="form-control",
	    title="Requirements",
	    placeholder="Requirements",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="requirements",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="splits",
	    label="Splits:",
	    bind=prc.optional,
	    class="form-control",
	    title="Splits",
	    placeholder="Splits",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="splits",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="title",
	    label="Title:",
	    bind=prc.optional,
	    class="form-control",
	    title="Title",
	    placeholder="Title",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="title",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="whichorder",
	    label="Whichorder:",
	    bind=prc.optional,
	    class="form-control",
	    title="Whichorder",
	    placeholder="Whichorder",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="whichorder",
	    groupWrapper="div class=form-group"
	)#



			
				

	<!--- Convert Entity To Fields 
	#html.entityFields( 
		entity=prc.optional,
		groupWrapper="div class='form-group'",
		class="form-control",
		fieldWrapper="",
		labelWrapper="",
		textAreas="",
		booleanSelect=true,
		manyToOne={},
		manyToMany={},
		showRelations=true
	)#--->
	
	<!--- Submit --->
	<div class="form-group">
	#html.href( href="cbadmin.module.bluFish.optionals", text="Cancel", class="btn btn-default" )#
	#html.submitButton(value="Save", class="btn btn-primary")#
	</div>

#html.endForm()#
</cfoutput>