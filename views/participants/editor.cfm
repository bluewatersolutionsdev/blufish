<!--- 
	EntityField Comment:
	- textAreas = A list of properties that are textareas
	- booleanSelect = If true creates a select box, else two radio buttons
	- manyToOne = { manyToOnePropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder=""} }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string]. Example: {criteria={productid=1},sortorder='Department desc'}
	- ManyToMany = { manyToManyPropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder="",selectColumn='' }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string,selectColumn='']. Example: {criteria={productid=1},sortorder='Department desc'}
	- showRelations = If true (default) will show one to many and one to one relations as a view table snapshot
--->

<cfoutput>
<div class="row">
    <div class="col-md-12">
        <h2 class="h3"><a href=""><i class="fa fa-users"></i> Participant:</a> #args.title#</h2>
    </div>
</div>
<cfset ptype = [
    {"name"= "Captain", "value"= "True"}
    , {"name"= "no", "value"= "False"}
] />
<!--- Submit Form --->
#html.startForm( action='cbadmin.module.bluFish.participants.save', name="modalForm" )#
<div class="row">
<div class="col-md-6">


#html.textField(
	    name="firstname",
	    label="First Name:",
	    bind=prc.participant,
	    class="form-control",
	    title="First Name",
	    placeholder="First Name",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="firstname",
	     groupWrapper="div class='form-group col-md-6'"
	)#

	#html.textField(
	    name="lastname",
	    label="Last Name:",
	    bind=prc.participant,
	    class="form-control",
	    title="Last Name",
	    placeholder="Last Name",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="lastname",
	     groupWrapper="div class='form-group col-md-6'"
	)#



	#html.textField(
	    name="address",
	    label="Address:",
	    bind=prc.participant,
	    class="form-control",
	    title="Address",
	    placeholder="Address",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="address",
	     groupWrapper="div class='form-group col-md-12'"
	)#




	#html.textField(
	    name="city",
	    label="City:",
	    bind=prc.participant,
	    class="form-control",
	    title="City",
	    placeholder="City",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="city",
	    groupWrapper="div class='form-group col-md-5'"
	)#

	#html.textField(
	    name="state",
	    label="State:",
	    bind=prc.participant,
	    class="form-control",
	    title="State",
	    placeholder="State",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="state",
	    groupWrapper="div class='form-group col-md-3'"
	)#



	



	#html.textField(
	    name="zip",
	    label="Zip:",
	    bind=prc.participant,
	    class="form-control",
	    title="Zip",
	    placeholder="Zip",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="zip",
	    groupWrapper="div class='form-group col-md-4'"
	)#


	#html.textField(
	    name="phone",
	    label="Phone:",
	    bind=prc.participant,
	    class="form-control",
	    title="Phone",
	    placeholder="Phone",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="phone",
	   groupWrapper="div class='form-group col-md-6'"
	)#


	#html.textField(
	    name="email",
	    label="Email:",
	    bind=prc.participant,
	    class="form-control",
	    title="Email",
	    placeholder="Email",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="email",
	    groupWrapper="div class='form-group col-md-12'"
	)#

	


			</div>	
	<div class="col-md-6">
<!---#html.select(
		name="boat", 
		label="boat:",
		bind=prc.participant,
		options=prc.boats,
		column="id",
		nameColumn="boat",
		class="form-control",
	    title="boat",
	    placeholder="boat",
	   groupWrapper="div class='form-group col-md-12'",
	    labelClass="control-label"
	)#--->
#html.select(
		name="type", 
		label="Type:",
		bind=prc.participant,
		options="Captain,Mate,Angler",
		class="form-control",
	    title="Type",
	    placeholder="Type",
	   groupWrapper="div class='form-group col-md-12'",
	    labelClass="control-label"
	)#


</div>

	#html.hiddenField(
	    name="id",
	    bind=prc.participant
	)#


	#html.hiddenField(
	    name="boat",
	    value=rc.boatid
	    
	)#

	#html.hiddenField(
	    name="tid",
	    value=rc.tid
	    
	)#





			
				

	<!--- Convert Entity To Fields 
	#html.entityFields( 
		entity=prc.participant,
		groupWrapper="div class='form-group'",
		class="form-control",
		fieldWrapper="",
		labelWrapper="",
		textAreas="",
		booleanSelect=true,
		manyToOne={},
		manyToMany={},
		showRelations=true
	)#--->
	
	<!--- Submit 
	<div class="form-group">
	#html.href( href="cbadmin.module.bluFish.participants", text="Cancel", class="btn btn-default" )#
	#html.submitButton(value="Save", class="btn btn-primary")#
	</div>--->
</div>
#html.endForm()#
</cfoutput>