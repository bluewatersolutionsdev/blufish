<cfoutput>
<!--- MessageBox --->
<cfif flash.exists( "notice" )>
<div class="row">
    <div class="col-md-12">
	    <div class="alert alert-#flash.get( "notice" ).type#">
	        #flash.get( "notice" ).message#
	    </div>
    </div>
</div>
</cfif>



<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
<!--- Listing --->
<fieldset>
<legend><i class="fa fa-users fa-lg"></i> Participants
#html.href( href="cbadmin.module.bluFish.participants.new", queryString="tid=#rc.tid#&boatid=#rc.boatid#", text="Add Participant", class="btn btn-primary pull-right",  "data-toggle"="modal", "data-target"="##myModal", "data-remote"="false")#
</legend>

<table class="sortable table table-striped table-hover table-condensed" name="crudlist" id="crudlist" >
	<thead>
		<tr>

		  <th class="hidden-xs">Boat</th>
			<th>Type</th>
      <th>Name</th>
			<th class="hidden-xs">eMail</th>
			<th class="hidden-xs">Phone</th>


			<th >Actions</th>
		</tr>
	</thead>
	<tbody>
  

		<cfloop array="#prc.participants#" index="thisRecord">
		<tr>
			
					<td class="hidden-xs">#thisRecord.getBOAT().getboat()#</td>
          <td>#thisRecord.gettype()#</td>
          <td>#thisRecord.getfirstname()# #thisRecord.getlastname()#</td>
          <td class="hidden-xs">#thisRecord.getemail()#</td>
          <td class="hidden-xs">#thisRecord.getphone()#</td>


			<td>
			<div class="btn-group pull-right">
			#html.startForm(action="cbadmin.module.bluFish.participants.delete", id="modalFormDelete")#
      #html.hiddenField(
      name="boatid",
      value="#thisRecord.getBOAT().getid()#"
      )#
      #html.hiddenField(
      name="tid",
      value="#rc.tid#"
      )#
				#html.button(value="<i class='fa fa-edit'></i> Delete", onclick="return confirm('Really Delete Record?')", class="btn btn-danger btn-sm hidden-xs", type="submit")# 		

				#html.href(href="cbadmin.module.bluFish.participants.edit", queryString="tid=#rc.tid#&boatid=#rc.boatid#&id=#thisRecord.getid()#", text="<i class='fa fa-edit'></i> Edit", class="btn btn-primary btn-sm", "data-toggle"="modal", "data-target"="##myModal", "data-remote"="false")#
				
				#html.hiddenField(name="id", bind=thisRecord)#
			#html.endForm()#
			</div>
			</td>
		</tr>
		</cfloop>
   
	</tbody>
</table>


</fieldset>

            </div>
        </div>
    </div>
</div>

<div class="row">


  #html.submitButton(
    value="Save & Next", 
    class="btn btn-primary pull-right",
    groupWrapper="div class='form-group col-md-12'"
  )#
  </div>


<script type="text/javascript">
$(document).ready(function(){

   var table = $('.sortable').DataTable({
       fixedHeader: true,
        stateSave: true,
        responsive: true
      });

    
    
    /*!
     * Modal placeholder action
     */
    $('##btnSave').click(function(event){
        $('##modalForm').submit();
        return false;
    });

    /*!
     * Modal placeholder for delete
     */
    $("##myModalDelete").on("show.bs.modal", function(e) {
 
    });

    /*!
     * Modal placeholder delete action
     */
    $('##btnDelete').click(function(event){
        $('##modalFormDelete').submit();
        return false;
    });



        
});


</script>







<div id="myModalDelete" class="modal fade modal-danger" role="dialog">
  <div class="modal-dialog" style="width:500px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Delete</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this item?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button name="btnDelete" type="button" class="btn btn-danger"  id="btnDelete">Delete</button>
      </div>
    </div>

  </div>
</div>
</cfoutput>