<cfoutput>
<link  href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" type="text/css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<link   href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
<script  src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>


<script src="//cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){

   var table = $('.sortable').DataTable({
       fixedHeader: true,
        stateSave: true,
        responsive: true
      });

 
  $("##propertyForm").validate();
  $("select").select2();

    /*!
     * Modal placeholder for quick edits
     */
    $("##myModal").on("show.bs.modal", function(e) {
      var loadingTitle='Loading...'
      var loadingBody='<i class="fa fa-spinner fa-spin fa-1x fa-fw" aria-hidden="true"></i>'
      var link = $(e.relatedTarget);
      $(this).find(".modal-title").html(loadingTitle);
      $(this).find(".modal-body").html(loadingBody);
      $(this).find(".modal-title").html(link.attr("href"));
      $(this).find(".modal-body").load(link.attr("href"));
    });
    
    /*!
     * Modal placeholder action
     */
    $('##btnSave').click(function(event){
        $('##modalForm').submit();
        return false;
    });
jQuery(document).keypress(function(e) {
  if (e.keyCode == 27) {
   jQuery("##myModal").modal('toggle');
                 OR
   jQuery("##myModal").modal('hide');
  }
 });
    /*!
     * Modal placeholder for delete
     */
    $("##myModalDelete").on("show.bs.modal", function(e) {
      var loadingTitle='Confirm Delete'
      var loadingBody='<i class="fa fa-spinner fa-spin fa-1x fa-fw" aria-hidden="true"></i>'
      var link = $(e.relatedTarget);
      $(this).find(".modal-title").html(loadingTitle);
      $(this).find(".modal-body").html(loadingBody);
      $(this).find(".modal-title").html(link.attr("title"));
      $(this).find(".modal-body").load(link.attr("href"));  
    });

    /*!
     * Modal placeholder delete action
     */
    $('##btnDelete').click(function(event){
        $('##modalFormDelete').submit();
        return false;
    });

        
        
    /*!
     * Remove Flags
     */
    function removeFlag(flagID){
      var $flagForm = $( "##flagForm" );
      $( "##delete_"+ flagID).removeClass( "fa-trash-o" ).addClass( "fa-spin fa-spinner" );
      checkAll( false, 'flagID' );
      $flagForm.find( "##flagID" ).val( flagID );
      $flagForm.submit();
    }


        
});





$(window).load(function(){
  $('[data-toggle="tabajax"]').click(function(e) {
    var $this = $(this),
        loadurl = $this.attr('href'),
        targ = $this.attr('data-target');
    $.get(loadurl, function(data) {
        $(targ).html(data);
    });
    $this.tab('show');
    return false;
	});
<cfif #structKeyExists(rc,"tab")#>
  $('###rc.tab# [data-toggle="tabajax"]').trigger('click');
</cfif>
});
</script>
</cfoutput>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:90%">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title hidden-xs">...</h4>
      </div>
      <div class="modal-body">
        <p>Loading...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button name="btnSave" type="button" class="btn btn-danger"  id="btnSave">Save</button>
      </div>
    </div>

  </div>
</div>






<div id="myModalDelete" class="modal fade modal-danger" role="dialog">
  <div class="modal-dialog" style="width:500px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Delete</h4>
      </div>
      <div class="modal-body">
        <p>Loading...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button name="btnDelete" type="button" class="btn btn-danger"  id="btnDelete">Delete</button>
      </div>
    </div>

  </div>
</div>