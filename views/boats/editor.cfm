<!--- 
	EntityField Comment:
	- textAreas = A list of properties that are textareas
	- booleanSelect = If true creates a select box, else two radio buttons
	- manyToOne = { manyToOnePropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder=""} }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string]. Example: {criteria={productid=1},sortorder='Department desc'}
	- ManyToMany = { manyToManyPropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder="",selectColumn='' }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string,selectColumn='']. Example: {criteria={productid=1},sortorder='Department desc'}
	- showRelations = If true (default) will show one to many and one to one relations as a view table snapshot
--->

<cfoutput>
<!---
<div class="row">
    <div class="col-md-12">
        <h2 class="h3"><a href=""><i class="fa fa-home"></i> boats:</a> #args.title#</h2>
    </div>
</div>
--->

<!--- Submit Form --->
#html.startForm( action='cbadmin.module.bluFish.boats.save' )#
	
<div class="row">
<div class="col-md-6">
<fieldset>
<legend><i class="fa fa-ship fa-lg"></i> Boat Specs</legend>
#html.textField(
	    name="boat",
	    label="Boat Name:",
	    bind=prc.boat,
	    class="form-control",
	    title="Boat Name",
	    placeholder="Boat Name",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="boat",
	      groupWrapper="div class='form-group col-md-12'"
	)#



	#html.textField(
	    name="boatbeam",
	    label="Boat Beam:",
	    bind=prc.boat,
	    class="form-control",
	    title="Boat Beam:",
	    placeholder="Boatbeam",
	    wrapper="div class=controls ",
	    labelClass="control-label",
	    id="boatbeam",
	    groupWrapper="div class='form-group col-md-6'"
	)#



	#html.textField(
	    name="boatlength",
	    label="Boat Length:",
	    bind=prc.boat,
	    class="form-control",
	    title="Boat Length",
	    placeholder="Boat Length",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="boatlength",
	    groupWrapper="div class='form-group col-md-6'"
	)#



	#html.textField(
	    name="boatmake",
	    label="Boat Make:",
	    bind=prc.boat,
	    class="form-control",
	    title="Boat Make",
	    placeholder="Boat Make",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="boatmake",
	   groupWrapper="div class='form-group col-md-6'"
	)#



	#html.textField(
	    name="boatyearbuilt",
	    label="Year Built:",
	    bind=prc.boat,
	    class="form-control",
	    title="Year Built",
	    placeholder="Year Built",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="boatyearbuilt",
	   groupWrapper="div class='form-group col-md-6'"
	)#

	#html.textField(
	    name="shorepower",
	    label="Shore Power:",
	    bind=prc.boat,
	    class="form-control",
	    title="Shore Power",
	    placeholder="Shore Power",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="shorepower",
	    groupWrapper="div class='form-group col-md-6'"
	)#

	#html.textField(
	    name="homeport",
	    label="Home Port:",
	    bind=prc.boat,
	    class="form-control",
	    title="Home Port",
	    placeholder="Home Port",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="homeport",
	    groupWrapper="div class='form-group col-md-6'"
	)#

	</fieldset>

<fieldset>
<legend><i class="fa fa-cogs fa-lg"></i> Engine Specs</legend>

	#html.textField(
	    name="enginemake",
	    label="Engine Make:",
	    bind=prc.boat,
	    class="form-control",
	    title="Engine Make",
	    placeholder="Engine Make",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="enginemake",
	   groupWrapper="div class='form-group col-md-4'"
	)#



	#html.textField(
	    name="enginemodel",
	    label="Engine Model:",
	    bind=prc.boat,
	    class="form-control",
	    title="Engine Model",
	    placeholder="Engine Model",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="enginemodel",
	   groupWrapper="div class='form-group col-md-4'"
	)#



	#html.textField(
	    name="enginepower",
	    label="Engine Power:",
	    bind=prc.boat,
	    class="form-control",
	    title="Engine Power",
	    placeholder="Engine Power",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="enginepower",
	    groupWrapper="div class='form-group col-md-4'"
	)#


	</fieldset>
</div>
<div class="col-md-6">

<fieldset>
<legend><i class="fa fa-user fa-lg"></i> Owner Info</legend>
#html.textField(
	    name="firstname",
	    label="First Name:",
	    bind=prc.boat,
	    class="form-control",
	    title="First Name",
	    placeholder="First Name",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="firstname",
	     groupWrapper="div class='form-group col-md-6'"
	)#

	#html.textField(
	    name="lastname",
	    label="Last Name:",
	    bind=prc.boat,
	    class="form-control",
	    title="Last Name",
	    placeholder="Last Name",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="lastname",
	     groupWrapper="div class='form-group col-md-6'"
	)#



	#html.textField(
	    name="address",
	    label="Address:",
	    bind=prc.boat,
	    class="form-control",
	    title="Address",
	    placeholder="Address",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="address",
	     groupWrapper="div class='form-group col-md-12'"
	)#




	#html.textField(
	    name="city",
	    label="City:",
	    bind=prc.boat,
	    class="form-control",
	    title="City",
	    placeholder="City",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="city",
	    groupWrapper="div class='form-group col-md-5'"
	)#

	#html.textField(
	    name="state",
	    label="State:",
	    bind=prc.boat,
	    class="form-control",
	    title="State",
	    placeholder="State",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="state",
	    groupWrapper="div class='form-group col-md-3'"
	)#



	



	#html.textField(
	    name="zip",
	    label="Zip:",
	    bind=prc.boat,
	    class="form-control",
	    title="Zip",
	    placeholder="Zip",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="zip",
	    groupWrapper="div class='form-group col-md-4'"
	)#


	#html.textField(
	    name="phone",
	    label="Phone:",
	    bind=prc.boat,
	    class="form-control",
	    title="Phone",
	    placeholder="Phone",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="phone",
	   groupWrapper="div class='form-group col-md-4'"
	)#


	#html.textField(
	    name="cell",
	    label="Cell:",
	    bind=prc.boat,
	    class="form-control",
	    title="Cell",
	    placeholder="Cell",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="cell",
	    groupWrapper="div class='form-group col-md-4'"
	)#


	#html.textField(
	    name="fax",
	    label="Fax:",
	    bind=prc.boat,
	    class="form-control",
	    title="Fax",
	    placeholder="Fax",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="fax",
	   groupWrapper="div class='form-group col-md-4'"
	)#

	#html.textField(
	    name="email",
	    label="Email:",
	    bind=prc.boat,
	    class="form-control",
	    title="Email",
	    placeholder="Email",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="email",
	    groupWrapper="div class='form-group col-md-12'"
	)#

	
</fieldset>


			</div>		

</div>

	#html.hiddenField(
	    name="id",
	    bind=prc.boat
	)#



	#html.hiddenField(
	    name="tid",
	    value=rc.tid
	)#


	



	
<!---


	





	



	
	#html.textField(
	    name="slip",
	    label="Slip:",
	    bind=prc.boat,
	    class="form-control",
	    title="Slip",
	    placeholder="Slip",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="slip",
	    groupWrapper="div class=form-group"
	)#


	#html.textField(
	    name="websiteorder",
	    label="Websiteorder:",
	    bind=prc.boat,
	    class="form-control",
	    title="Websiteorder",
	    placeholder="Websiteorder",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="websiteorder",
	    groupWrapper="div class=form-group"
	)#


	#html.textField(
	    name="approved",
	    label="Approved:",
	    bind=prc.boat,
	    class="form-control",
	    title="Approved",
	    placeholder="Approved",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="approved",
	    groupWrapper="div class=form-group"
	)#
			
--->

	<!--- Convert Entity To Fields 
	#html.entityFields( 
		entity=prc.boat,
		groupWrapper="div class='form-group'",
		class="form-control",
		fieldWrapper="",
		labelWrapper="",
		textAreas="",
		booleanSelect=true,
		manyToOne={},
		manyToMany={},
		showRelations=true
	)#--->
	
	<!--- Submit --->
	<div class="row">


	#html.submitButton(
		value="Save & Next", 
		class="btn btn-primary pull-right",
		groupWrapper="div class='form-group col-md-12'"
	)#
	</div>

#html.endForm()#
</cfoutput>