<!--- 
	EntityField Comment:
	- textAreas = A list of properties that are textareas
	- booleanSelect = If true creates a select box, else two radio buttons
	- manyToOne = { manyToOnePropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder=""} }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string]. Example: {criteria={productid=1},sortorder='Department desc'}
	- ManyToMany = { manyToManyPropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder="",selectColumn='' }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string,selectColumn='']. Example: {criteria={productid=1},sortorder='Department desc'}
	- showRelations = If true (default) will show one to many and one to one relations as a view table snapshot
--->

<cfoutput>
<div class="row">
    <div class="col-md-12">
        <h2 class="h3"><a href=""><i class="fa fa-home"></i> Tournament: Categories:</a> #args.title#</h2>
    </div>
</div>

<!--- Submit Form --->
#html.startForm( action='cbadmin.module.bluFish.categories.save' )#
	


	#html.textField(
	    name="categoryname",
	    label="Categoryname:",
	    bind=prc.category,
	    class="form-control",
	    title="Categoryname",
	    placeholder="Categoryname",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="categoryname",
	    groupWrapper="div class=form-group"
	)#



	#html.hiddenField(
	    name="id",
	    bind=prc.category
	)#

	#html.hiddenField(
	    name="tournament",
	    value=rc.tid
	)#



	#html.textField(
	    name="maxoptionalplaces",
	    label="Maxoptionalplaces:",
	    bind=prc.category,
	    class="form-control",
	    title="Maxoptionalplaces",
	    placeholder="Maxoptionalplaces",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="maxoptionalplaces",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="maxplaces",
	    label="Maxplaces:",
	    bind=prc.category,
	    class="form-control",
	    title="Maxplaces",
	    placeholder="Maxplaces",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="maxplaces",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="optionalsplits",
	    label="Optionalsplits:",
	    bind=prc.category,
	    class="form-control",
	    title="Optionalsplits",
	    placeholder="Optionalsplits",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="optionalsplits",
	    groupWrapper="div class=form-group"
	)#





	#html.textField(
	    name="tournamentsplits",
	    label="Tournamentsplits:",
	    bind=prc.category,
	    class="form-control",
	    title="Tournamentsplits",
	    placeholder="Tournamentsplits",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="tournamentsplits",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="whichorder",
	    label="Whichorder:",
	    bind=prc.category,
	    class="form-control",
	    title="Whichorder",
	    placeholder="Whichorder",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="whichorder",
	    groupWrapper="div class=form-group"
	)#



			
				

	<!--- Convert Entity To Fields 
	#html.entityFields( 
		entity=prc.category,
		groupWrapper="div class='form-group'",
		class="form-control",
		fieldWrapper="",
		labelWrapper="",
		textAreas="",
		booleanSelect=true,
		manyToOne={},
		manyToMany={},
		showRelations=true
	)#--->
	
	<!--- Submit --->
	<div class="form-group">
	#html.href( href="cbadmin.module.bluFish.categories", text="Cancel", class="btn btn-default" )#
	#html.submitButton(value="Save", class="btn btn-primary")#
	</div>

#html.endForm()#
</cfoutput>