<cfoutput>
<div class="row hidden-sm">
    <div class="col-md-6">
        <h2 class="h3"><a href=""><i class="fa fa-home"></i> Tournament:</a>  Categories</h2>
    </div>
    <div class="col-md-6">
        <!--- Create Button --->
#html.href( 
  href="cbadmin.module.bluFish.categories.new", 
  queryString="tid=#rc.tid#", 
  text="Create Category", 
  class="btn btn-primary pull-right"
)#
    </div>
</div>


<!--- MessageBox --->
<cfif flash.exists( "notice" )>
<div class="row">
    <div class="col-md-12">
	    <div class="alert alert-#flash.get( "notice" ).type#">
	        #flash.get( "notice" ).message#
	    </div>
    </div>
</div>
</cfif>



<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
<!--- Listing --->

<table class="sortable table table-striped table-hover table-condensed" name="crudlist" id="crudlist" >
	<thead>
		<tr>
		
			<th>Order</th>
			
			<th>Category</th>
			<th>Max. Places</th>
			<th>Tournament Splits</th>
			
			<th>Max. Optional Places</th>
			<th>Optional Splits</th>
			<th width="250">Actions</th>
		</tr>
	</thead>
	<tbody>
		<cfloop array="#prc.categories#" index="thisRecord">
		<tr>
  			  <td>#thisRecord.getwhichorder()#</td>
					<td>#thisRecord.getcategoryname()#</td>
          <td>#thisRecord.getmaxplaces()#</td>
          <td>#thisRecord.gettournamentsplits()#</td>
					<td>#thisRecord.getmaxoptionalplaces()#</td>
          <td>#thisRecord.getoptionalsplits()#</td>
					
					
			
			<td>
			<div class="btn-group pull-right">
			#html.startForm(action="cbadmin.module.bluFish.categories.delete", id="modalFormDelete")#
				#html.button(value="<i class='fa fa-edit'></i> Delete", onclick="return confirm('Really Delete Record?')", class="btn btn-danger btn-sm", type="submit")# 		

				#html.href(href="cbadmin.module.bluFish.optionals.index", queryString="tid=#rc.tid#&cid=#thisRecord.getid()#", text="<i class='fa fa-edit'></i> Optionals", class="btn btn-primary btn-sm")#

        #html.href(href="cbadmin.module.bluFish.categories.edit", queryString="tid=#rc.tid#&id=#thisRecord.getid()#", text="<i class='fa fa-edit'></i> Edit", class="btn btn-primary btn-sm")#
				
				#html.hiddenField(name="id", bind=thisRecord)#
			#html.endForm()#
			</div>
			</td>
		</tr>
		</cfloop>
	</tbody>
</table>


            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function(){

   var table = $('.sortable').DataTable({
       fixedHeader: true,
        stateSave: true,
        responsive: true
      });

    
    
    /*!
     * Modal placeholder action
     */
    $('##btnSave').click(function(event){
        $('##modalForm').submit();
        return false;
    });

    /*!
     * Modal placeholder for delete
     */
    $("##myModalDelete").on("show.bs.modal", function(e) {
 
    });

    /*!
     * Modal placeholder delete action
     */
    $('##btnDelete').click(function(event){
        $('##modalFormDelete').submit();
        return false;
    });



        
});


</script>







<div id="myModalDelete" class="modal fade modal-danger" role="dialog">
  <div class="modal-dialog" style="width:500px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Delete</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this item?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button name="btnDelete" type="button" class="btn btn-danger"  id="btnDelete">Delete</button>
      </div>
    </div>

  </div>
</div>
</cfoutput>