<cfoutput>
<!---
<cfinclude template="../_tags/helpers.cfm">
--->
<div class="row">
    <div class="col-md-12">
        <h2 class="h3"><a href="#prc.cbhelper.buildmodulelink(module="blufish", linkto="properties")#"><i class="fa fa-home"></i> Properties:</a> #prc.qProperties.propname#</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!--- MessageBox --->
        #getModel( "messagebox@cbMessagebox" ).renderit()#

        <!--- Logs --->
        <cfif flash.exists( "forgeboxInstallLog" )>
            <h3>Installation Log</h3>
            <div class="consoleLog">#flash.get( "forgeboxInstallLog" )#</div>
        </cfif>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">

				<div class="tab-wrapper tab-primary">
	                    <!-- Tabs -->
	                    <ul class="nav nav-tabs">
	                        <li class="active" title="Details" 	id="tDetails">
	                            <a href="#prc.cbhelper.buildmodulelink(module="blufish", linkto="properties/editForm/propid/" & prc.qProperties.propid)#" 	data-toggle="tabajax" data-target="##tab_details"><i class="fa fa-info-circle fa-lg"></i> Details</a>
	                        </li>
	                        <cfif prc.oAuthor.checkPermission( "blufish_ADMIN,blufish_PHOTOS" )>
	                        <li class="" title="Photos"	id="tPhotos">
	                            <a href="#prc.cbhelper.buildmodulelink(module="blufish", linkto="properties/photos/propid/" & prc.qProperties.propid)#" 	data-toggle="tabajax" data-target="##tab_photos"><i class="fa fa-camera fa-lg"></i> Photos</a>
	                        </li>
	                        </cfif>
	                         <cfif prc.oAuthor.checkPermission( "blufish_ADMIN,blufish_AMENITIES" )>
	                        <li class="" title="Amenities"	id="tAmenities">
	                            <a href="#prc.cbhelper.buildmodulelink(module="blufish", linkto="properties/amenities/propid/" & prc.qProperties.propid)#" 	data-toggle="tabajax" data-target="##tab_amenities"><i class="fa fa-camera fa-lg"></i> Amenities</a>
	                        </li>
	                        </cfif>
	                        <cfif prc.oAuthor.checkPermission( "blufish_ADMIN,blufish_RATES" )>
	                        <li class="" title="Rates" 	id="tRates">
	                            <a href="#prc.cbhelper.buildmodulelink(module="blufish", linkto="properties/rates/propid/" & prc.qProperties.propid)#" 		data-toggle="tabajax" data-target="##tab_rates"><i class="fa fa-user fa-lg"></i> Rates</a>
	                        </li>
	                        </cfif>
	                        <cfif prc.oAuthor.checkPermission( "blufish_ADMIN,blufish_SPECIALS" )>
	                        <li class="" title="Specials" id="tSpecials">
	                            <a href="#prc.cbhelper.buildmodulelink(module="blufish", linkto="properties/specials/propid/" & prc.qProperties.propid)#"	 data-toggle="tabajax" data-target="##tab_specials"><i class="fa fa-certificate fa-lg"></i> Specials</a>
	                        </li>
	                        </cfif>
	                        <cfif prc.oAuthor.checkPermission( "blufish_ADMIN" )>
	                        <li class="" title="Maps" id="tMaps">
	                            <a href="#prc.cbhelper.buildmodulelink(module="blufish", linkto="properties/map/propid/" & prc.qProperties.propid)#" 		data-toggle="tabajax" data-target="##tab_maps"><i class="fa fa-globe fa-lg"></i> Maps</a>
	                        </li>
	                        </cfif>
	                        <cfif prc.oAuthor.checkPermission( "blufish_ADMIN,blufish_FLAGS" )>
	                        <li class="" title="Flags" id="tFlags">
	                            <a href="#prc.cbhelper.buildmodulelink(module="blufish", linkto="properties/flags/propid/" & prc.qProperties.propid)#" 		data-toggle="tabajax" data-target="##tab_flags"><i class="fa fa-tags fa-lg"></i> Flags</a>
	                        </li>
	                        </cfif>
	                        <cfif prc.oAuthor.checkPermission( "blufish_ADMIN,blufish_OASSESSMENTS" )>
	                        <li class="" title="Owner Assessments" id="toAssessments">
	                            <a href="#prc.cbhelper.buildmodulelink(module="blufish", linkto="properties/oassessments/propid/" & prc.qProperties.propid)#" 		data-toggle="tabajax" data-target="##tab_oassessments"><i class="fa fa-users fa-lg"></i> Owner</a>
	                        </li>
	                        </cfif>
	                    </ul>
	                    <div class="tab-content">
	                        <!-- Tab ` -->
	                        <div id="tab_details" class="tab-pane active">   

	                        	<cfinclude template="editForm.cfm">                         
	  			                            
	                        </div>
	                        <!-- Tab ` -->
	                        <div id="tab_photos" class="tab-pane">                                
	  			            	<h2 class="h1">                                
	  			             		<i class="fa fa-spinner fa-spin fa-1x fa-fw" aria-hidden="true"></i>   Loading...
	  			            	</h2><br clear="all">        
	                        </div>
	                        <!-- Tab ` -->
	                        <div id="tab_amenities" class="tab-pane">                                
	  			            	<h2 class="h1">                                
	  			             		<i class="fa fa-spinner fa-spin fa-1x fa-fw" aria-hidden="true"></i>   Loading...
	  			            	</h2><br clear="all">        
	                        </div>
	                        <!-- Tab ` -->
	                        <div id="tab_rates" class="tab-pane">
	                        	<h2 class="h1">                                
	  			             		<i class="fa fa-spinner fa-spin fa-1x fa-fw" aria-hidden="true"></i>   Loading...
	  			            	</h2><br clear="all">
	                        </div>
	                        <!-- Tab ` -->
	                        <div id="tab_specials" class="tab-pane">   
	                        	<h2 class="h1">                                
	  			             		<i class="fa fa-spinner fa-spin fa-1x fa-fw" aria-hidden="true"></i>   Loading...
	  			            	</h2><br clear="all">                           
	  			                      
	                        </div>
	                        <!-- Tab ` -->
	                        <div id="tab_maps" class="tab-pane">                                
		                    	<h2 class="h1">                                
	  			             		<i class="fa fa-spinner fa-spin fa-1x fa-fw" aria-hidden="true"></i>   Loading...
	  			            	</h2><br clear="all">
	                        </div>
							<!-- Tab ` -->
	                        <div id="tab_flags" class="tab-pane">   
	                        	<h2 class="h1">                                
	  			             		<i class="fa fa-spinner fa-spin fa-1x fa-fw" aria-hidden="true"></i>   Loading...
	  			            	</h2><br clear="all">                           
	  			                      
	                        </div>
	                        <!-- Tab ` -->
	                        <div id="tab_oassessments" class="tab-pane">   
	                        	<h2 class="h1">                                
	  			             		<i class="fa fa-spinner fa-spin fa-1x fa-fw" aria-hidden="true"></i>   Loading...
	  			            	</h2><br clear="all">                           
	  			                      
	                        </div>

	                        <!--- end manage pane --->
	                       
	                    </div>
	                    <!-- End Tab Content -->


	                

	            </div>
            </div>
        </div>
    </div>
    
    </div>
</div>
</cfoutput>