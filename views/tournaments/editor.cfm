<!--- 
	EntityField Comment:
	- textAreas = A list of properties that are textareas
	- booleanSelect = If true creates a select box, else two radio buttons
	- manyToOne = { manyToOnePropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder=""} }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string]. Example: {criteria={productid=1},sortorder='Department desc'}
	- ManyToMany = { manyToManyPropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder="",selectColumn='' }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string,selectColumn='']. Example: {criteria={productid=1},sortorder='Department desc'}
	- showRelations = If true (default) will show one to many and one to one relations as a view table snapshot
--->

<cfoutput>
<div class="row">
    <div class="col-md-12">
        <h2 class="h3"><a href=""><i class="fa fa-home"></i> tournaments:</a> #args.title#</h2>
    </div>
</div>

<!--- Submit Form --->
#html.startForm( action='cbadmin.module.bluFish.tournaments.save' )#
	


	#html.textField(
	    name="active",
	    label="Active:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Active",
	    placeholder="Active",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="active",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="creditcardfee",
	    label="Creditcardfee:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Creditcardfee",
	    placeholder="Creditcardfee",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="creditcardfee",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="displaylocked",
	    label="Displaylocked:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Displaylocked",
	    placeholder="Displaylocked",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="displaylocked",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="entryfee",
	    label="Entryfee:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Entryfee",
	    placeholder="Entryfee",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="entryfee",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="entryfeecut",
	    label="Entryfeecut:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Entryfeecut",
	    placeholder="Entryfeecut",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="entryfeecut",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="entryfeetotal",
	    label="Entryfeetotal:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Entryfeetotal",
	    placeholder="Entryfeetotal",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="entryfeetotal",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="fishminimum",
	    label="Fishminimum:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Fishminimum",
	    placeholder="Fishminimum",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="fishminimum",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="fishmultiplier",
	    label="Fishmultiplier:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Fishmultiplier",
	    placeholder="Fishmultiplier",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="fishmultiplier",
	    groupWrapper="div class=form-group"
	)#



	#html.hiddenField(
	    name="ID",
	    bind=prc.tournament
	)#

	



	#html.textField(
	    name="legalattachment",
	    label="Legalattachment:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Legalattachment",
	    placeholder="Legalattachment",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="legalattachment",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="maxoptionallevels",
	    label="Maxoptionallevels:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Maxoptionallevels",
	    placeholder="Maxoptionallevels",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="maxoptionallevels",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="optionalfeecut",
	    label="Optionalfeecut:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Optionalfeecut",
	    placeholder="Optionalfeecut",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="optionalfeecut",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="taxrate",
	    label="Taxrate:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Taxrate",
	    placeholder="Taxrate",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="taxrate",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="tournamentname",
	    label="tournamentname:",
	    bind=prc.tournament,
	    class="form-control",
	    title="tournamentname",
	    placeholder="tournamentname",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="tournamentname",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="tournamentstyle",
	    label="Tournamentstyle:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Tournamentstyle",
	    placeholder="Tournamentstyle",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="tournamentstyle",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="tournamentyear",
	    label="Tournamentyear:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Tournamentyear",
	    placeholder="Tournamentyear",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="tournamentyear",
	    groupWrapper="div class=form-group"
	)#



	#html.textField(
	    name="whichorder",
	    label="Whichorder:",
	    bind=prc.tournament,
	    class="form-control",
	    title="Whichorder",
	    placeholder="Whichorder",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="whichorder",
	    groupWrapper="div class=form-group"
	)#



			
				

	<!--- Convert Entity To Fields
	#html.entityFields( 
		entity=prc.tournament,
		groupWrapper="div class='form-group'",
		class="form-control",
		fieldWrapper="",
		labelWrapper="",
		textAreas="",
		booleanSelect=true,
		manyToOne={},
		manyToMany={},
		showRelations=true
	)#--->
	
	<!--- Submit --->
	<div class="form-group">
	#html.href( href="cbadmin.module.bluFish.tournaments", text="Cancel", class="btn btn-default" )#
	#html.submitButton(value="Save", class="btn btn-primary")#
	</div>

#html.endForm()#
</cfoutput>