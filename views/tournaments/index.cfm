<cfoutput>
<div class="row hidden-sm">
    <div class="col-md-6">
        <h2 class="h3"><a href=""><i class="fa fa-home"></i> Registration:</a>  Tournaments</h2>
    </div>
    <div class="col-md-6">
        <!--- Create Button --->
        #html.href(
          href="cbadmin.module.bluFish.tournaments.new", 
          text="Create Tournament", 
          class="btn btn-primary pull-right"
        )#
    </div>
</div>


<!--- MessageBox --->
<cfif flash.exists( "notice" )>
<div class="row">
    <div class="col-md-12">
	    <div class="alert alert-#flash.get( "notice" ).type#">
	        #flash.get( "notice" ).message#
	    </div>
    </div>
</div>
</cfif>



<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
<!--- Listing --->

<table class="sortable table table-striped table-hover table-condensed" name="crudlist" id="crudlist" >
	<thead>
		<tr>
			<th>Active</th>
			<th>Name</th>
			<th>Year</th>
			<th>Entry Fee</th>
			<th>Tax Rate</th>
			<th width="300">Actions</th>
		</tr>
	</thead>
	<tbody>
		<cfloop array="#prc.tournaments#" index="thisRecord">
		<tr>
			<td>#thisRecord.getactive()#</td>
			<td>#thisRecord.gettournamentname()#</td>
			<td>#thisRecord.gettournamentyear()#</td>
			<td>#thisRecord.getentryfee()#</td>
			<td>#thisRecord.gettaxrate()#</td>
			<td>
  			<div class="btn-group pull-right">
    			#html.startForm(
            action="cbadmin.module.bluFish.tournaments.delete", 
            id="modalFormDelete
          ")#
    				#html.button(
              value="<i class='fa fa-edit'></i> Delete", 
              onclick="return confirm('Really Delete Record?')", 
              class="btn btn-danger btn-sm", type="submit"
            )# 		
            #html.href(
              href="cbadmin.module.bluFish.categories.index", 
              queryString="tid=#thisRecord.getid()#", 
              text="<i class='fa fa-bars'></i> Categories", 
              class="btn btn-primary btn-sm"
            )#
            #html.href(
              href="cbadmin.module.bluFish.boats.index", 
              queryString="tid=#thisRecord.getid()#", 
              text="<i class='fa fa-ship'></i> Boats", 
              class="btn btn-primary btn-sm"
            )#
    				#html.href(
              href="cbadmin.module.bluFish.tournaments.edit", 
              queryString="id=#thisRecord.getid()#", 
              text="<i class='fa fa-edit'></i> Edit", 
              class="btn btn-primary btn-sm"
            )#
    				#html.hiddenField(
              name="id", 
              bind=thisRecord
            )#
    			#html.endForm()#
  			</div>
			</td>
		</tr>
		</cfloop>
	</tbody>
</table>


            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function(){

   var table = $('.sortable').DataTable({
       fixedHeader: true,
        stateSave: true,
        responsive: true
      });

    
    
    /*!
     * Modal placeholder action
     */
    $('##btnSave').click(function(event){
        $('##modalForm').submit();
        return false;
    });
     
});


</script>
</cfoutput>