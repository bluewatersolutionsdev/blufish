<!--- 
	EntityField Comment:
	- textAreas = A list of properties that are textareas
	- booleanSelect = If true creates a select box, else two radio buttons
	- manyToOne = { manyToOnePropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder=""} }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string]. Example: {criteria={productid=1},sortorder='Department desc'}
	- ManyToMany = { manyToManyPropertyName = {valuecolumn='',namecolumn='',criteria={},sortorder="",selectColumn='' }
		A structure of data to help with many to one relationships on how they are presented. 
		Possible key values for each key are [valuecolumn='',namecolumn='',criteria={},sortorder=string,selectColumn='']. Example: {criteria={productid=1},sortorder='Department desc'}
	- showRelations = If true (default) will show one to many and one to one relations as a view table snapshot
--->

<cfoutput>
<div class="row">
    <div class="col-md-12">
        <h2 class="h3"><a href=""><i class="fa fa-home"></i> invitations:</a> #args.title#</h2>
    </div>
</div>

<!--- Submit Form --->
#html.startForm( action='cbadmin.module.bluFish.invitations.save' )#
	




	#html.textField(
	    name="expirationdate",
	    label="Expirationdate:",
	    bind=prc.invitation,
	    class="form-control",
	    title="Expirationdate",
	    placeholder="Expirationdate",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="expirationdate",
	    groupWrapper="div class=form-group"
	)#



	#html.hiddenField(
	    name="id",
	    bind=prc.invitation
	)#



	#html.textField(
	    name="invitationcode",
	    label="Invitationcode:",
	    bind=prc.invitation,
	    class="form-control",
	    title="Invitationcode",
	    placeholder="Invitationcode",
	    wrapper="div class=controls",
	    labelClass="control-label",
	    id="invitationcode",
	    groupWrapper="div class=form-group"
	)#





			
				

	<!--- Convert Entity To Fields 
	#html.entityFields( 
		entity=prc.invitation,
		groupWrapper="div class='form-group'",
		class="form-control",
		fieldWrapper="",
		labelWrapper="",
		textAreas="",
		booleanSelect=true,
		manyToOne={},
		manyToMany={},
		showRelations=true
	)#--->
	
	<!--- Submit --->
	<div class="form-group">
	#html.href( href="cbadmin.module.bluFish.invitations", text="Cancel", class="btn btn-default" )#
	#html.submitButton(value="Save", class="btn btn-primary")#
	</div>

#html.endForm()#
</cfoutput>